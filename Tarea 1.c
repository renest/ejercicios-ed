#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//Determina si un numero es par o impar
void par(int numero){
	numero = numero%2;
	
	if(numero == 0){
		printf("Es par\n");
	}
	else{
		printf("Es impar\n");
	}
}

//Muestra la función de Fibonacci desde el 0 hasta mostar la suma de 89.
void fibonacci(){
	int numeroActual = 0;
	int numeroSucesor = 1;
	
	for (int i = 0;i<10;i++){
		int resultado = numeroActual + numeroSucesor;
		printf("%d\n",resultado);

		numeroActual = numeroSucesor; //Número actual ahora toma el valor del número sucesor.
		numeroSucesor = resultado; //Numero sucesor es el resultado de la suma de esta vuelta.
	}
}

//Muestra el factorial de un número
void factorial(){
	int numero = 0;
	printf("¿De cual número quiere saber el factorial?\n");
	scanf("%d",&numero); 
	int resultado = 1;
	int posicion = numero; //posicion me sirve para poder restar el número sin perder el valor del numero original entrante.
	for (int i = 0;i < numero;i++){
		resultado = resultado * posicion;
		posicion = posicion - 1;
	}
	printf("El factorial de %d es %d\n",numero,resultado);
}

//Suma numeros usando la función for.
void sumaFor(){
	int numero = 0;
	printf("¿Hasta que número desea sumar?\n");
	scanf("%d",&numero);
	int resultado = 0;
	int posicion = 1; 
	for (int i = 0; i<numero;i++){
		resultado = resultado + posicion;
		posicion = posicion + 1;
	}
	printf("El resultado de la suma es %d\n",resultado);
}

//Suma numeros usando la función while.
void sumaWhile(){
	int numero = 0;
	printf("¿Hasta que número desea sumar?\n");
	scanf("%d",&numero);
	int resultado = 0;
	int i = 0;
	int posicion = 1; 
	while (i<numero){
		resultado = resultado + posicion;
		posicion = posicion + 1;
		i++;
	}
	printf("El resultado de la suma es %d\n",resultado);

}

//Deternina la cantidad de dígitos que tiene un número con while.
void largoNumeroWhile(){
	char numero[20];
	int largo = 0;
	char *p; //Puntero que va a apuntar a numero[]
	printf("Ingrese un número\n");
	scanf("%s",numero);

	p = numero;

	while(*p != '\0'){ // '\0' significa si ha llegado al fin de la cadena de chars. 
		largo++; //Suma a largo
		p++; //Pasa al siguiente char
	}

	printf("La longitud de %s es de %d dígitos\n" ,numero,largo);
}

//Deternina la cantidad de dígitos que tiene un número con for.
void largoNumeroFor(){
	char numero[20];
	int largo = 0;
	char *p; //Puntero que va a apuntar a numero[]
	printf("Ingrese un número\n");
	scanf("%s",numero);

	p = numero;

	for(p;*p != '\0';p++){ // '\0' significa si ha llegado al fin de la cadena de chars. 
		largo++; //Suma a largo
	}

	printf("La longitud de %s es de %d dígitos\n" ,numero,largo);
}

//Invierte los dígitos del número
void invertirNumero(){
	char numero [20];
	char reversa [20];
	int largo = 0;	

	printf("Ingrese el número que desea invertir\n");
	scanf("%s", numero); 
	
	largo = strlen(numero); //Determina lo largo de la cadena de chars		
	for (int i=0; i<largo ;i++) { 
		reversa[i] = numero[largo - i]; //Recorre todos los chars y despúes en reversa los almacena en la nueva variable "reversa".
	}
	printf("%s\n",reversa);
}

//Determina si el número es lo mismo de izquierda a derecha, que de derecha a izquierda.
void palindormo(){
	//char numero[20];
	char numero [20];
	char reversa [20];
	int largo = 0;	
	int k = 0;
	int pal = 1; //Servirá como verdadero o falso para verificar si es palíndromo.

	printf("Ingrese el número que desea verificar si es palíndormo\n");
	scanf("%s", numero); 
	
	largo = strlen(numero); //Determina lo largo de la cadena de chars		
	for (int i=0; i<largo ;i++) { 
		reversa[i] = numero[largo - i]; //Recorre todos los chars y despúes en reversa los almacena en la nueva variable "reversa".
	}

	while(k<largo){
		if (numero[k] == reversa[k]){
			k++; //Suma a k para que siga recorriendo la cadena de chars.
		}else{
			pal = 0; //Estado que verifica que no es palíndromo.
		}
	}

	//Imprimirá si es o no es palíndromo dependiendo del while de arriba.
	if (pal == 1){
		printf("Es palíndromo\n");
	}else{
		printf("No es palíndromo\n");
	}
}


int main(int argc, char const *argv[])
{
	int opcion = 0;
	printf("¿Cuál opción desea?\n 1- Determinar si un número es par o impar\n 2- Fibonacci\n 3- Factorial\n 4- Largo de un número FOR\n 5- Largo de un número WHILE\n 6- Suma con For\n 7- Suma con While\n 8-Invertir un numero\n 9- Determinar si un númereo es palíndormo\n");
	scanf("%d",&opcion);

	switch(opcion) //Switch es como un if
	{
		case 1: //Si la opción es 1
		par(5);		
		break;

		case 2: //Si la opción es 2
		fibonacci();
		break;

		case 3: //Si la opción es 3		
		factorial();
		break;

		case 4:
		largoNumeroFor();
		break;

		case 5: //Si la opción es 5
		largoNumeroWhile();
		break;

		case 6:
		sumaFor();
		break;

		case 7:
		sumaWhile();
		break;

		case 8:
		invertirNumero();
		break;

		case 9:
		palindormo();
		break;
	}
	return 0;
}
