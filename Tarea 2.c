#include <stdio.h>
#include <string.h>
#include <stdlib.h>

//Elimina un caracter específico de una palabra.
void eliminarCaracter(){
	char texto[5] = "hola";
	char caracter = 'a';

	int i = 0;
	char borrar = ' '; //Se reemplazaría por el caracter que queremos borrer.

	while (texto[i] != '\0'){
		if (texto[i] == caracter){
			texto[i] = borrar;
		}else{
			i++;
		}
	}
	printf("%s\n",texto);
}

//Invierte el orden de una palabra.
void invertir(){
	char texto[5] = "hola";
	char textoReversa[5] = "";

	int largo = 0;

	largo = strlen(texto);

	for (int i = 0;i<largo;i++){
		textoReversa[largo-i-1] = texto[i]; //la posicion de texto reversa sería en este caso, en la primera vuelta 4 (largo)-i(0)-1(se le resta 1 para que no cuente la posición del '\0')
	}

	printf("El texto invertido es %s\n",textoReversa);
}

//Pasa la variable de la memoria de literale al Heap.
void textoEnHeap(){
	char *texto = "Hola mundo";
	char *textoEnHeap = calloc(12,sizeof(char));

	strcpy(textoEnHeap,texto); //Copia lo que hay en la variable texto y la pega en textoEnHeap.

	printf("El texto es %s\n",textoEnHeap);
}

int main(int argc, char const *argv[])
{
	int opcion = 0;

	printf("Menú:\n 1- Eliminar caracter.\n 2- Invertir un texto.\n 3- Copiar un texto en el HEAP.\n");
	scanf("%d",&opcion);
	switch(opcion){

		case 1:
		eliminarCaracter();
		break;

		case 2:
		invertir();
		break;

		case 3:
		textoEnHeap();
		break;
	}
	

	return 0;
}